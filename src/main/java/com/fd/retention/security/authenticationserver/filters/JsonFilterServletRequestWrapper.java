package com.fd.retention.security.authenticationserver.filters;

import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.springframework.security.web.savedrequest.Enumerator;

/**
 * Subclass of HttpRequestWrapper to support JSON credentials for token requests.
 * Parses the JSON in request body to get credentials and grant_type, 
 * and put them with the original request parameter into a new HashMap.
 * Then, override method of getParameterValues, getParameter, getParameterNames and getParameterMap
 * to return values from that new HashMap.
 * 
 * @author e019904
 */
public class JsonFilterServletRequestWrapper extends HttpServletRequestWrapper {
	private final Map<String, String[]> params;
	
	public JsonFilterServletRequestWrapper(HttpServletRequest request, Map<String, String[]> inParams) {
		super(request);
		params = inParams;
	}

    @Override
    public String getParameter(String name) {
        if (this.params.containsKey(name)) {
            return this.params.get(name)[0];
        }
        return "";
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return this.params;
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return new Enumerator<>(params.keySet());
    }

    @Override
    public String[] getParameterValues(String name) {
        return params.get(name);
    }	
}
