package com.fd.retention.security.authenticationserver.filters;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.connector.RequestFacade;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This filter allows the credentials to be provided via application/json.
 * In the case that the client makes the token request sending application/json rather than "application/x-www-form-urlencoded"
 * we still want to honor the request.  (oauth2 spec indicates that Access token request should use application/x-www-form-urlencoded.)
 * 
 * Spring Security post filter for credentials retrieves the credentials from request parameters.
 * Therefore, we pass them from JSON body into the request parameter using this filter.
 * The idea is like follows:
 * 	1. Create a custom spring security filter.
 * 	2. In your custom filter, create a class to subclass HttpRequestWrapper. 
 *     The class allow you to wrap the original request and get parameters from JSON.
 *	3. In your subclass of HttpRequestWrapper, parse your JSON in request body to get credentials and grant_type, 
 *     and put them with the original request parameter into a new HashMap. Then, override method of getParameterValues, 
 *     getParameter, getParameterNames and getParameterMap to return values from that new HashMap
 *  4. Pass your wrapped request into the filter chain.
 *  5. Configure your custom filter in your Spring Security Config.
 * 
 * @author e019904
 */
@Component
@Order(value = Integer.MIN_VALUE)
public class JsonToUrlEncodedAuthenticationFilter implements Filter 
{
	@Override
	public void init(FilterConfig filterConfig) throws ServletException 
	{
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException 
	{
        if (Objects.equals(request.getContentType(), "application/json") 
        	&& Objects.equals(((RequestFacade) request).getServletPath(), "/oauth/token")) 
        {
            InputStream is = request.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            byte[] json = buffer.toByteArray();

            Map<String, Object> result = new ObjectMapper().readValue(json, new TypeReference<Map<String, Object>>(){});
            Map<String, String[]> r = new HashMap<>();
            for (String key : result.keySet()) {
                String[] val = new String[1];
                val[0] = (String)result.get(key);
                r.put(key, val);
            }

            String[] val = new String[1];
            val[0] = ((RequestFacade) request).getMethod();
            r.put("_method", val);

            HttpServletRequest s = new JsonFilterServletRequestWrapper(((HttpServletRequest) request), r);
            chain.doFilter(s, response);
        } else {
            chain.doFilter(request, response);
        }
	}

	@Override
	public void destroy() 
	{
	}
}
