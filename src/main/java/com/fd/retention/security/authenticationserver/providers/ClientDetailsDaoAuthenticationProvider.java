package com.fd.retention.security.authenticationserver.providers;

import java.util.Set;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import com.fd.retention.security.authenticationserver.providers.client.ClientIdSecretAudienceAuthenticationToken;
import com.fd.retention.security.authenticationserver.providers.client.ClientUser;

/**
 * Extend the DaoAuthenticationProvider by adding a check for audience matching the client resources.
 *  
 * @author e019904
 */
public class ClientDetailsDaoAuthenticationProvider extends DaoAuthenticationProvider {
	/**
	 * 
	 */
	public ClientDetailsDaoAuthenticationProvider() {
		super();
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (ClientIdSecretAudienceAuthenticationToken.class.isAssignableFrom(authentication));
	}
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
		UsernamePasswordAuthenticationToken authentication) throws AuthenticationException 	
	{
		checkAudience(userDetails, authentication);
		super.additionalAuthenticationChecks(userDetails, authentication);
	}

	/**
	 * Validate the client requested audience is valid for the client.
	 * @param userDetails UserDetails (instance of ClientUser)
	 * @param authentication UsernamePasswordAuthenticationToken (instance of UsernamePasswordAudienceAuthenticationToken)
	 */
	protected void checkAudience(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) {
		if(userDetails instanceof ClientUser && authentication instanceof ClientIdSecretAudienceAuthenticationToken) 
		{
			String requestedAudience = ((ClientIdSecretAudienceAuthenticationToken)authentication).getAudience();
			Set<String> resources = ((ClientUser)userDetails).getResources();
			if( !resources.stream().anyMatch(resource -> resource.equals(requestedAudience)) ) {
				logger.debug("Authentication failed: audience does not match stored value");

				throw new BadCredentialsException("Bad credentials - invalid audience");				
			}
		}
		else {
			logger.debug("Authentication failed: Not able to validate audience, incorrect authentication type");
			
			throw new BadCredentialsException("Bad Credentials - missing audience");			
		}
	}	
}
