package com.fd.retention.security.authenticationserver.providers.auth0;

/**
 * Interface for accessing Auth0
 * 
 * @author e019904
 */
public interface Auth0Client {

	String getToken(TokenRequest request) throws Exception;
}