/**
 * 
 */
package com.fd.retention.security.authenticationserver.providers.client;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * Add storage of the audience (unique identifier of the target API you want to access).
 * 
 * @author e019904
 */
public class ClientIdSecretAudienceAuthenticationToken extends UsernamePasswordAuthenticationToken {
	/** */
	private static final long serialVersionUID = 7285272230410113493L;

	private String audience;
	
	/**
	 * @param principal
	 * @param credentials
	 */
	public ClientIdSecretAudienceAuthenticationToken(Object principal, Object credentials) {
		super(principal, credentials);
	}

	/**
	 * @param principal
	 * @param credentials
	 * @param authorities
	 */
	public ClientIdSecretAudienceAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
	}

	/**
	 * @param principal
	 * @param credentials
	 * @param audience
	 */
	public ClientIdSecretAudienceAuthenticationToken(Object principal, Object credentials, String audience) {
		super(principal, credentials);
		this.audience = audience;
	}
	
	/**
	 * @return String
	 */
	public final String getAudience() {
		return audience;
	}
	
}
