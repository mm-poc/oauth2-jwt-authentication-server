/**
 * 
 */
package com.fd.retention.security.authenticationserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * 
 */
@Configuration
@EnableAuthorizationServer
public class AuthEntrypointConfig extends AuthorizationServerConfigurerAdapter {
    @Bean
    public AuthenticationEntryPoint oauthAuthenticationEntryPoint() {
    	OAuth2AuthenticationEntryPoint aep = new OAuth2AuthenticationEntryPoint();
    	aep.setRealmName("theRealm");
    	return aep;    	
    }
    @Bean
    public AuthenticationEntryPoint clientAuthenticationEntryPoint() {
    	OAuth2AuthenticationEntryPoint aep = new OAuth2AuthenticationEntryPoint();
    	aep.setRealmName("theRealm/client");
    	return aep;    	
    }
    
    @Bean
    public AccessDeniedHandler oauthAccessDeniedHandler() {
    	return new OAuth2AccessDeniedHandler();
    }
}
