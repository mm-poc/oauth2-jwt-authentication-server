package com.fd.retention.security.authenticationserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Configure the http request authentication requirements.
 * 
 * Use the WebSecurityConfigurerAdapter to turn “off” the default security options such as 
 * close all accesses to our end points.  We’ll correctly configure the accesses later on.
 * 
 * @author e019904
 */
@Configuration
@EnableWebSecurity( debug = false )
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
	    	.csrf().disable()
	        .anonymous().disable() // disable anonymous user
	        .formLogin().disable() // disable form authentication
	        .authorizeRequests().anyRequest().denyAll()	// denying all access
			;
	}
}
