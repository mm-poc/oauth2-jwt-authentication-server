/**
 * 
 */
package com.fd.retention.security.authenticationserver.providers.client;

import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * Extends User to store the resources for the client (for client_credentials).
 * 
 * @author e019904
 */
public class ClientUser extends User {
	/** */
	private static final long serialVersionUID = -7058605933499163817L;

	private Set<String> resources;

	/**
	 * @param username
	 * @param password
	 * @param authorities
	 */
	public ClientUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Set<String> resources) {
		super(username, password, authorities);
		this.resources = resources;
	}

	/**
	 * @param username
	 * @param password
	 * @param enabled
	 * @param accountNonExpired
	 * @param credentialsNonExpired
	 * @param accountNonLocked
	 * @param authorities
	 */
	public ClientUser(String username, String password, boolean enabled, boolean accountNonExpired, 
		boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, Set<String> resources) 
	{
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.resources = resources;
	}

	/**
	 * @return Set<String>
	 */
	public final Set<String> getResources() {
		return resources;
	}
}
