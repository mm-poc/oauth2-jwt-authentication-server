package com.fd.retention.security.authenticationserver.filters;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import com.fd.retention.security.authenticationserver.providers.client.ClientIdSecretAudienceAuthenticationToken;

/**
 * Extend ClientCredentialsTokenEndpointFilter to pass audience along for validation in the authentication process.
 * 
 * @author e019904
 */
public class CustomClientCredentialsTokenEndpointFilter extends ClientCredentialsTokenEndpointFilter {
	private boolean allowOnlyPost = false;

	/** Constructor */
	public CustomClientCredentialsTokenEndpointFilter() {
		super();
	}

	/**
	 * Constructor
	 * @param path
	 */
	public CustomClientCredentialsTokenEndpointFilter(String path) {
		super(path);
	}

	@Override
	public void setAllowOnlyPost(boolean allowOnlyPost) {
		this.allowOnlyPost = allowOnlyPost;
		super.setAllowOnlyPost(allowOnlyPost);
	}

	/**
	 * @return boolean
	 */
	protected boolean isAllowOnlyPost() {
		return allowOnlyPost;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		if (isAllowOnlyPost() && !"POST".equalsIgnoreCase(request.getMethod())) {
			throw new HttpRequestMethodNotSupportedException(request.getMethod(), new String[] { "POST" });
		}

		String clientId = request.getParameter("client_id");
		String clientSecret = request.getParameter("client_secret");
		String audience = request.getParameter("audience");

		// If the request is already authenticated we can assume that this filter is not needed
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.isAuthenticated()) {
			return authentication;
		}

		if (clientId == null) {
			throw new BadCredentialsException("No client credentials presented");
		}

		if (clientSecret == null) {
			clientSecret = "";
		}

		clientId = clientId.trim();
		UsernamePasswordAuthenticationToken authRequest = new ClientIdSecretAudienceAuthenticationToken(clientId, clientSecret, audience);

		return this.getAuthenticationManager().authenticate(authRequest);
	}
}