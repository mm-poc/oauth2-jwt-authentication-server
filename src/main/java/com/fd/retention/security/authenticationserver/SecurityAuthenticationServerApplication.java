package com.fd.retention.security.authenticationserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.fd"})
@SpringBootApplication
public class SecurityAuthenticationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityAuthenticationServerApplication.class, args);
	}

}
