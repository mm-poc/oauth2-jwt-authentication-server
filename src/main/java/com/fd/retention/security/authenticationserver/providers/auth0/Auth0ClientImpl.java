package com.fd.retention.security.authenticationserver.providers.auth0;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fd.common.rest.client.RestClientHelper;

/**
 * Provide integration service for calling Auth0.
 * 
 * @author e019904
 */
@Service
public class Auth0ClientImpl implements HealthIndicator, Auth0Client {
    private static final Logger logger = LoggerFactory.getLogger(Auth0ClientImpl.class);
    private static final String TOKEN_RESOURCE = "oauth/token";
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String HEADER_APPLICATION_JSON =  "application/json";
    
	@Value("${jwt.auth0.token-default.provider:https://localhost/}")
	private String host;
	
	@Autowired
	private RestClientHelper restClientHelper;
    
    @Override
    public Health health() 
    {
    	logger.debug("CHECK HEALTH: Auth0Client ...");
    	Health health = null;
    	
		//check auth0 host.
		if (host != null && host.equals("https://localhost/"))
		{
			logger.error("HEALTHCHECK FAILURE: Auth0Client - config server failed to load auth0 host. Restarting application...");
        	health = Health.down().withDetail("Health Error with Auth0Client", host).build();
		}
		else
		{
			logger.debug("HEALTHCHECK OK: Auth0Client");			
        	health = Health.up().build(); 
        }
        return health;
    }
	
    /**
     * 
     */
    public String getToken(TokenRequest request) throws Exception {
		String payload = new ObjectMapper().writeValueAsString(request);
		HttpHeaders headers = new HttpHeaders();		
		headers.add(HEADER_CONTENT_TYPE, HEADER_APPLICATION_JSON);

		ResponseEntity<String> responseEntity = restClientHelper.post(host, false, TOKEN_RESOURCE, payload, headers);
    	
    	return responseEntity.getBody();    	
    }    
}
