package com.fd.retention.security.authenticationserver.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter;

import com.fd.retention.security.authenticationserver.filters.CustomClientCredentialsTokenEndpointFilter;
import com.fd.retention.security.authenticationserver.providers.Auth0ClientAuthenticationProvider;
import com.fd.retention.security.authenticationserver.providers.ClientDetailsDaoAuthenticationProvider;
import com.fd.retention.security.authenticationserver.providers.client.CustomClientDetailsUserDetailsService;

/**
 * Spring will give us an authentication server, providing standard Oauth2 tokens at the endpoint /oauth/token.
 * This class provides an AuthorizationServerConfigurerAdapter bean to correctly setup the authorization server.
 * 
 * Specifically: 
 * 		Configure the client details service and authentication providers.
 * 
 * @author e019904
 */
@Configuration
@EnableAuthorizationServer
public class AuthenticationServerConfig  extends AuthorizationServerConfigurerAdapter {	    
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.addTokenEndpointAuthenticationFilter(clientCredentialsTokenEndpointFilter());
	}

	@Bean
	protected ClientCredentialsTokenEndpointFilter clientCredentialsTokenEndpointFilter() {
		ClientCredentialsTokenEndpointFilter cctef = new CustomClientCredentialsTokenEndpointFilter();		 
		cctef.setAuthenticationManager(clientAuthenticationManager());
		return cctef;
	}

	@Bean
	protected ProviderManager clientAuthenticationManager() {
		return new ProviderManager(Arrays.asList(auth0ClientProvider(), authProvider()));
	}
	
	@Bean
	protected DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new ClientDetailsDaoAuthenticationProvider();
        authProvider.setUserDetailsService(clientDetailsUserService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }
	
	@Bean
	protected Auth0ClientAuthenticationProvider auth0ClientProvider() {
		Auth0ClientAuthenticationProvider provider = new Auth0ClientAuthenticationProvider();
		return provider;
	}

	@Bean
	protected BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	    
    @Bean
    protected UserDetailsService clientDetailsUserService() {
    	return new CustomClientDetailsUserDetailsService(clientDetailsService());
    }
    
    @Bean
    protected ClientDetailsService clientDetailsService() {    	
    	return new ClientDetailsService() {
			@Override
			public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
				BaseClientDetails details = new BaseClientDetails();
		        details.setClientId("barClientIdPassword");
		        details.setClientSecret(passwordEncoder().encode("secret"));
		        details.setAuthorizedGrantTypes(Arrays.asList("client_credentials"));
		        details.setScope(Arrays.asList("read", "trust"));
		        details.setResourceIds(Arrays.asList("kip-apis"));
		        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		        authorities.add(new SimpleGrantedAuthority("ROLE_CLIENT"));
		        details.setAuthorities(authorities);
		        details.setAccessTokenValiditySeconds(3600);	//1hr
		        details.setRegisteredRedirectUri(null);
		        return details;
			}
		};
    }
}
