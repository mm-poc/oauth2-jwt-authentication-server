package com.fd.retention.security.authenticationserver.providers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.fd.retention.security.authenticationserver.providers.auth0.Auth0Client;
import com.fd.retention.security.authenticationserver.providers.auth0.TokenRequest;
import com.fd.retention.security.authenticationserver.providers.client.ClientIdSecretAudienceAuthenticationToken;

/**
 * Interface with Auth0 to authenticate the client.
 *  
 * @author e019904
 */
public class Auth0ClientAuthenticationProvider implements AuthenticationProvider {
	private static final Logger logger = LoggerFactory.getLogger(Auth0ClientAuthenticationProvider.class);
	@Autowired
	private Auth0Client auth0Client;
	
	/** Constructor */
	public Auth0ClientAuthenticationProvider() {
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (ClientIdSecretAudienceAuthenticationToken.class.isAssignableFrom(authentication));
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String clientId = (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
		String secret = (authentication.getCredentials() == null) ? "" : authentication.getCredentials().toString();		
		
		TokenRequest request = new TokenRequest();
		request.setGrant_type("client_credentials");
		request.setClient_id(clientId);
		request.setClient_secret(secret);
		try {
			String result = auth0Client.getToken(request);
			logger.debug("RESULT = " + result);
			
		}
		catch (Exception e) {
			throw new AuthenticationServiceException("Error in Auth0", e);
		}
		
		throw new AuthenticationServiceException("Auth0 not found");
		//return createSuccessAuthentication(clientId, authentication);
	}

	/**
	 * Creates a successful {@link Authentication} object.
	 * Store the original credentials the user supplied (not salted or encoded passwords) 
	 * in the returned <code>Authentication</code> object.
	 *
	 * @param principal that should be the principal in the returned object (defined by
	 * the {@link #isForcePrincipalAsString()} method)
	 * @param authentication that was presented to the provider for validation
	 * @param user that was loaded by the implementation
	 *
	 * @return the successful authentication token
	 */
	protected Authentication createSuccessAuthentication(Object principal, Authentication authentication) {
		// Ensure we return the original credentials the user supplied,
		// so subsequent attempts are successful even with encoded passwords.
		// Also ensure we return the original getDetails(), so that future
		// authentication events after cache expiry contain the details
		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
			principal, authentication.getCredentials(), null);
		result.setDetails(authentication.getDetails());

		return result;
	}
}
