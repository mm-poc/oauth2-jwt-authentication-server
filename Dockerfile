FROM openjdk:8u111-jdk-alpine
LABEL "org.opencontainers.image.vendor"="1800Flowers.com"
VOLUME /tmp
COPY /target/jwtauthenticationserver-0.0.1.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]